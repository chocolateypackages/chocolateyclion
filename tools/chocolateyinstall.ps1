

$ErrorActionPreference = 'Stop';
 
 
$packageArgs = @{
  packageName    = 'JetBrains Clion'
  fileType       = 'EXE'
  softwareName   = 'JetBrains Clion Professional'
  url            = 'https://download.jetbrains.com/cpp/CLion-2016.2.3.exe'
  checksum       = '9f6038879daeecf7420d87ae6ca1d0c664f74b9e6e234a89a55fcacd1214cf6b'
  checksumType   = 'sha256'
  url64bit       = 'https://download.jetbrains.com/cpp/CLion-2016.2.3.exe'
  checksum64     = '9f6038879daeecf7420d87ae6ca1d0c664f74b9e6e234a89a55fcacd1214cf6b'
  checksumType64 = 'sha256'
  silentArgs     = '/S'
  validExitCodes = @(0)
}
 Install-ChocolateyPackage @packageArgs	
 
 
